from django.conf.urls.defaults import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'acrank.views.home', name='home'),

    url(r'^register/$', 'acrank.views.DriverRegistration', name='register'),
    

    url(r'^hotlaps/$', 'acrank.views.hotlaps', name='hotlaps'),

    url(r'^login/$', 'acrank.views.login', name='login'),
    url(r'^auth/$', 'acrank.views.auth_view'),
    url(r'^logout/$', 'acrank.views.logout', name='logout'),
    url(r'^invalid/$', 'acrank.views.invalid', name='invalid'),
    # url(r'^Ptyxiakh/', include('Ptyxiakh.foo.urls')),
    url(r'^StatsSend$', 'acrank.views_StatsSend.StatsSend', name='StatsSend'),          #Gia thn apostolh MONO twn stats
    url(r'^profile$', 'acrank.views_profile.profile', name='profile'),
    url(r'^myranking$', 'acrank.views.myranking', name='myranking'),

    url(r'^drivers$', 'acrank.views.drivers', name='drivers'),

    url(r'^validation$', 'acrank.views.validation', name='validation'),


    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
