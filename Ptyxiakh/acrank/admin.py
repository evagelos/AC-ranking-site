from django.contrib import admin
from acrank.models import driver
from acrank.models import DriverStatistics
admin.site.register(driver)
admin.site.register(DriverStatistics)
