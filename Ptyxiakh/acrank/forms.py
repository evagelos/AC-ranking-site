from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from acrank.models import driver
from acrank.models import DriverStatistics



class RegistrationForm(ModelForm):

        first_name = forms.CharField(label=(u'First Name'))
        last_name = forms.CharField(label=(u'Last Name'))
        username= forms.CharField(label=(u'Driver'))
        email = forms.EmailField(label=(u'Email Address'))
        password = forms.CharField(label=(u'Password'), widget=forms.PasswordInput(render_value=False))
        vpassword = forms.CharField(label=(u'Verify Password'), widget=forms.PasswordInput(render_value=False))
        
        
        
        
        class Meta:
                model = driver
                fields = ('username','email','password','vpassword','first_name','last_name','licenseKey')
                widgets = {'licenseKey': forms.PasswordInput(),}
	
        def clean_username(self):
                username = self.cleaned_data['username']
                try:
                        User.objects.get(username=username)
                except User.DoesNotExist:
                        return username
                raise forms.ValidationError('That username is already taken, please select another.')

        def clean_email(self):
                email = self.cleaned_data['email']
                try:
                        User.objects.get(email=email)
                except User.DoesNotExist:
                        return email
                raise forms.ValidationError('This email adress already exists,please try again.')

        def clean_vpassword(self):
                if 'password' in self.cleaned_data: 
                        password = self.cleaned_data['password']
                        vpassword = self.cleaned_data['vpassword']
                        if password == vpassword:
                                return vpassword
                raise forms.ValidationError('The passwords you entered do not match.')


######### Elegxos otan kanei update sta stoixeia tou profil tou #######################

class DriverProfileForm(ModelForm):

        class Meta:
                model = driver
                exclude = ('user','licenseKey')


######### Apostolh thlemetrias #######################

class StatisticsForm(ModelForm):

        
        licenseKey= forms.CharField(label=(u'Driver'))

        class Meta:
                model = DriverStatistics
                exclude = ('statisticsToDriver',)

        def clean_username(self):
                licenseKey = self.cleaned_data['licenseKey']
                try:
                        driver.objects.get(licenseKey=licenseKey)
                        return licenseKey
                except User.DoesNotExist: 
                        raise forms.ValidationError("That username is NOT registered.")

######### Elegxos otan anoigei to app sto game k tsekarei an einai eggegramenos sto site #######################

class ValidationForm(ModelForm):

        username= forms.CharField()
        licenseKey= forms.CharField()

        class Meta:
                model=driver
                fields=('licenseKey',)

        def clean_username_password(self):
                username = self.cleaned_data['username']
                licenseKey = self.cleaned_data['licenseKey']
                try:
                        user=User.objects.get(username=username)
                        driver.objects.get(user=user,licenseKey=licenseKey)
                        return username,licenseKey
                except User.DoesNotExist: 
                        raise forms.ValidationError("That username is NOT registered.")
