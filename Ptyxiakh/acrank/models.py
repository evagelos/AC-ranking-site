from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class driver(models.Model):

      user = models.OneToOneField(User)

      HARDWARE_CHOICES = (
            ('Keyboard', 'Keyboard'),
            ('Mouse', 'Mouse'),
            ('Keyboard/Mouse', 'Keyboard/Mouse'),
            ('Wheel', 'Wheel'),)
      COUNTRY_CHOICES = (
            ('GR', 'Greece'),
            ('EN', 'England'),
            ('FR', 'France'),
            ('IT', 'Italy'),)

      licenseKey=models.CharField(max_length=50)
      team=models.CharField(blank=True,max_length=30)
      country=models.CharField(blank=True,max_length=30,choices=COUNTRY_CHOICES)
      hardware=models.CharField(blank=True,max_length=30,choices=HARDWARE_CHOICES)
      age=models.DateField(blank=True,null=True,max_length=30)
      totallaps=models.IntegerField(blank=True,null=True,max_length=30)
      safe=models.IntegerField(blank=True,null=True,max_length=30)
      bio=models.TextField(blank=True,max_length=30)
      lookingFortTeam=models.NullBooleanField(blank=True,null=True,max_length=30)
      website=models.URLField(blank=True,null=True,max_length=30)
      contactInfo=models.EmailField(blank=True,null=True,max_length=30)

      def __unicode__(self):
                return self.user


class DriverStatistics(models.Model):

      statisticsToDriver=models.ForeignKey('driver')

      besttime = models.IntegerField()
      bestsplit1 = models.IntegerField()
      bestsplit2 = models.IntegerField()
      maxspeed = models.FloatField(max_length=5)
      datesubmitted=models.DateField(auto_now=True,auto_now_add=True)
      idealLine= models.BooleanField(default=False)
      autoBlip=models.BooleanField(default=False)
      stabilityControl=models.BooleanField(default=False)
      autoBrake=models.BooleanField(default=False)
      autoShifter=models.BooleanField(default=False)
      aBs=models.BooleanField(default=False)
      tractionControl=models.BooleanField(default=False)
      autoClutch=models.BooleanField(default=False)
      visualDamage=models.BooleanField(default=False)
      damage=models.BooleanField(default=False)
      fuelRate=models.BooleanField(default=False)
      tyreWear=models.BooleanField(default=False)

      def __unicode__(self):
                return self.statisticsToDriver.user



def create_driver_user_callback(sender, instance, **kwargs): 
      
	acrank, new = driver.objects.get_or_create(user=insance)
	post_save.connect(create_acrank_user_callback, User)

def create_DriverStatistics_user_callback(sender, instance, **kwargs):      
      
      acrank, new = DriverStatistics.objects.get_or_create(user=insance)
      post_save.connect(create_acrank_user_callback, User)