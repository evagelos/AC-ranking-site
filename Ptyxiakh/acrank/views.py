# Create your views here.
from django.shortcuts import render_to_response
from acrank.forms import RegistrationForm
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.template import RequestContext
from django.http import HttpResponse 						# Gia def validation
from django.views.decorators.csrf import csrf_exempt		# Gia def validation
from acrank.forms import ValidationForm						# Gia def validation
from acrank.models import driver
from acrank.models import DriverStatistics
from django.contrib import auth 							# GIa login...
from django.core.context_processors import csrf 			# GIa login...												
from django.core.mail import send_mail

							


def home(request):
	return render_to_response('index.html', context_instance=RequestContext(request))

def drivers(request):
	users = User.objects.all()
	profiles = driver.objects.all()
	entries = {'dict1': {'User': users} , 'dict2' : {'driver': profiles} }

	return render_to_response('drivers.html',{'User': users} ,context_instance=RequestContext(request))

################# Hotlaps(ksexwrista tou xrhsth) #####################

def myranking(request):
	
	if request.user.is_authenticated():

		user=request.user
		order_by_user = request.GET.get('order_by', 'besttime')
		tempentries = DriverStatistics.objects.filter(statisticsToDriver=user).order_by(order_by_user)
		if DriverStatistics.objects.filter(statisticsToDriver=user).count() > 0:
			theoritical=[]
			bestS1=DriverStatistics.objects.filter(statisticsToDriver=user).order_by('bestsplit1')
			bestS2=DriverStatistics.objects.filter(statisticsToDriver=user).order_by('bestsplit2')
			theoritical.append(bestS1[0].bestsplit1 + bestS2[0].bestsplit2)
			theoritical.append(bestS1[0].bestsplit1)
			theoritical.append(bestS2[0].bestsplit2)
		else:
			theoritical=[0,0,0]
		entries={'dict1':{'DriverStatistics':tempentries} , 'dict2': {'theoritical':theoritical} }
		return render_to_response('myranking.html', entries, context_instance=RequestContext(request))
	else:
		return HttpResponseRedirect('/')

################# Hotlaps(sunolika) #####################

def hotlaps(request):
	emptyList=[]
	best=[]	

	for users in User.objects.all():
		if DriverStatistics.objects.filter(statisticsToDriver=users.id).exists():										#Mphke to if giati o kainourgios xrhsths pou den eixe apothikeysei xronous ton epsaxnw k evgaze error
			emptyList.append(DriverStatistics.objects.filter(statisticsToDriver=users.id).order_by('besttime')[0])
		else:
			pass
		
	sortedList=sorted(emptyList, key=lambda DriverStatistics: DriverStatistics.besttime)
	
	for users in User.objects.all():
		if DriverStatistics.objects.filter(statisticsToDriver=users.id).exists():
			best.append(-sortedList[0].besttime)
		else:
			pass																			#Vazw to kalutero xrono se lista(me meion),gia na to prosthetw stous xronous twn allwn k na vriskw th diafora tous

	entries={'dict1':{'sortedLists':sortedList},'dict2':{'bests':best}}
	return render_to_response('hotlaps.html', entries, context_instance=RequestContext(request))

################# Registration.. #####################
	
def DriverRegistration(request):
	
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')

	if request.method == 'POST':
		form = RegistrationForm(request.POST)
		if form.is_valid():
			user = User.objects.create_user(username=form.cleaned_data['username'], email=form.cleaned_data['email'], password=form.cleaned_data['password'],first_name=form.cleaned_data['first_name'],last_name=form.cleaned_data['last_name'])
			user.save()
																				#Afou dhmiourghsw ton User
																											#Dhmiourgw k ton driver,ton swzw..
			tempuser=User.objects.get(username=user.username)																			#kai epeita ton "vriskw" sth vash k dhmiourgw k to arxiko
			acrank=driver.objects.create(user=tempuser,licenseKey=form.cleaned_data['licenseKey'])													#profil tou wste na tou dwsw to LicenseKey
			acrank.save()

			email_title='Welcome to ACRanking!'
			email_content="Welcome to ACRanking "+str(user.username)+"! Happy ranking!"
			send_mail(email_title, email_content, 'ACRanking@gmail.com',[user.email], fail_silently=False)

			return HttpResponseRedirect('/')
			

		else:
			return render_to_response('registration/register.html', {'form': form}, context_instance=RequestContext(request))

	else:
		form = RegistrationForm()
		context = {'form': form}
		return render_to_response('registration/register.html', context, context_instance=RequestContext(request))

#################Validation tou user prin xrhsimopoihsei to app sto game #####################

@csrf_exempt
def validation(request):
	form=ValidationForm(request.POST)

	if form.is_valid():
		if User.objects.filter(username=request.POST['username']).exists():
			user=User.objects.get(username=request.POST['username'])
			if user.driver.licenseKey==request.POST['licenseKey']:
				return HttpResponse(status=200)
			else:
				return HttpResponse(status=403)
		else:
			return HttpResponse(status=403)	
	else:
		return HttpResponse(status=400)
	
#################LOGIN/LOGOUT/INVALID etc #####################

def login(request):
	if request.user.is_authenticated():
		return HttpResponseRedirect('/')
	else:
		c = {}
		c.update(csrf(request))
		return render_to_response('login.html', c)

def auth_view(request):
	username = request.POST.get('username', '')
	password = request.POST.get('password', '')
	user = auth.authenticate(username=username, password=password)

	if user is not None:
		auth.login(request, user)
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/invalid')


def invalid(request):
	return render_to_response('invalid.html')

def logout(request):
	auth.logout(request)
	return render_to_response('logout.html')