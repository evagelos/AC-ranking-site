from acrank.forms import StatisticsForm
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from django.http import HttpResponse

from acrank.models import driver
from acrank.models import DriverStatistics


@csrf_exempt
def StatsSend(request):
	form = StatisticsForm(request.GET)

	if form.is_valid():
		#user1=driver.objects.get(licenseKey=form.cleaned_data['licenseKey'])
		user2 = get_object_or_404(driver, licenseKey=form.cleaned_data['licenseKey'])
		acrank= DriverStatistics.objects.create(statisticsToDriver=user2,
			besttime=form.cleaned_data['besttime'],
			bestsplit1=form.cleaned_data['bestsplit1'],
			bestsplit2=form.cleaned_data['bestsplit2'],
			maxspeed=form.cleaned_data['maxspeed'],
			idealLine=form.cleaned_data['idealLine'],
			autoBlip=form.cleaned_data['autoBlip'],
			stabilityControl=form.cleaned_data['stabilityControl'],
			autoBrake=form.cleaned_data['autoBrake'],
			autoShifter=form.cleaned_data['autoShifter'],
			aBs=form.cleaned_data['aBs'],
			tractionControl=form.cleaned_data['tractionControl'],
			autoClutch=form.cleaned_data['autoClutch'],
			visualDamage=form.cleaned_data['visualDamage'],
			damage=form.cleaned_data['damage'],
			fuelRate=form.cleaned_data['fuelRate'],
			tyreWear=form.cleaned_data['tyreWear'],
			)
		acrank.save()
		return HttpResponse(status=201)
	else:
		return HttpResponse(status=400)
