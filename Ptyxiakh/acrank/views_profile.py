# Create your views here.
from acrank.forms import DriverProfileForm
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.template import RequestContext
from acrank.models import driver
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.contrib import messages
@login_required
def profile(request):
	if request.method == 'POST':
		user=driver.objects.get(user=request.user)
		form = DriverProfileForm(request.POST, instance=user)
		if form.is_valid():		
			form.save()
			messages.success(request, 'Profile details updated.',extra_tags='alert alert-success')
			return HttpResponseRedirect('/profile')
		
		messages.warning(request, 'Invalid form!',extra_tags='alert alert-error')
		return HttpResponseRedirect('/profile')
	else:
		profileInfo=driver.objects.get(user=request.user)
		form = DriverProfileForm()
		context={ 'dict1' : {'form': form} , 'dict2' : {'driver': profileInfo} }
		return render_to_response('profile.html', context,context_instance=RequestContext(request))


	